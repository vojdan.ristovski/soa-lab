from fastapi import FastAPI
# from src.api.v1.api import router as api_v1_router

app = FastAPI()

@app.get("/")
async def root():
    return {"Hello" : "World"}

#app.include_router(api_v1_router, prefix="/api/v1")